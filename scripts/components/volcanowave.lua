
local VolcanoWave = Class(function(self, inst)
	self.inst = inst
	self.waves = inst.WaveComponent
	self.inst:StartUpdatingComponent(self)
end)

function VolcanoWave:OnUpdate()
	if self.waves and ThePlayer then
        local map = TheWorld.Map
		local px, py, pz = ThePlayer.Transform:GetWorldPosition()
		local x, y = map:GetTileXYAtPoint(px, py, pz)

        local disttolava = GetClosestTileDist(x, y, GROUND.IMPASSABLE, 20)
        --local disttocloud = map:GetClosestTileDist(x, y, GROUND.IMPASSABLE, 20)

        if disttolava <= 20 and ChangeToLava(px, py, pz) then
		   self.waves:SetWaveTexture( resolvefilepath("images/volcano_waves/lava_active.tex") )
		else
			self.waves:SetWaveTexture( resolvefilepath("images/volcano_waves/volcano_cloud.tex") )
		end
	end
end

return VolcanoWave