local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

-------------------------------------------------------------------------------------------

IAENV.AddClassPostConstruct("widgets/mapwidget", function (widget)
    local image = IsInIAClimate(widget.owner) and "_shipwrecked" or ""
    widget.bg:SetTexture("images/hud" .. image .. ".xml", "map".. image .. ".tex")
end)