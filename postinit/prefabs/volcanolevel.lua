local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

--[[
Experimental  code , is only valid when cave is not turned on -k

local function VolcanoLavaHandle(world)
    local map = world.Map
    local minimap = world.minimap.MiniMap
    local height, width = map:GetSize()
    local height_start, height_end = math.floor(height / 4), math.floor(height * 3 / 4)
    local width_start, width_end = math.floor(width / 4), math.floor(width * 3 / 4)
    local IMPASSABLE, VOLCANO_LAVA= GROUND.IMPASSABLE, GROUND.VOLCANO_LAVA
    for y = height_start, height_end do
        for x = width_start, width_end do
            local tile = map:GetTile(x, y)
            if tile == IMPASSABLE then
                map:SetTile(x, y, VOLCANO_LAVA)
                minimap:RebuildLayer(IMPASSABLE, x, y)
                minimap:RebuildLayer(VOLCANO_LAVA, x, y)

            end
        end
    end

    world:RemoveEventCallback("ms_playerjoined", VolcanoLavaHandle)
end

local function VolcanoLava(world)
    local map = world.Map
    local minimap = world.minimap.MiniMap
    local height, width = map:GetSize()
    local height_start, height_end = math.floor(height / 4), math.floor(height * 3 / 4)
    local width_start, width_end = math.floor(width / 4), math.floor(width * 3 / 4)
    local IMPASSABLE, VOLCANO_LAVA = GROUND.IMPASSABLE, GROUND.VOLCANO_LAVA

    for y = height_start, height_end do
        for x = width_start, width_end do
            local tile = map:GetTile(x, y)
            if tile == VOLCANO_LAVA then
                map:SetTile(x, y, IMPASSABLE)
                map:RebuildLayer(VOLCANO_LAVA, x, y)
                map:RebuildLayer(IMPASSABLE, x, y)

            end
        end
    end

    map:Finalize(0)

end
--]]
local Lava_Centre_X, Lava_Centre_Y, Lava_Centre_Radius = nil, nil , nil

IAENV.AddSimPostInit(function()
    local world = TheWorld
    local map = world.Map

    if not world:HasTag("volcano") then
        return
    end

    map:SetUndergroundFadeHeight(0)
    map:SetTransparentOcean(false)
    if not TheNet:IsDedicated() and world.WaveComponent then
        world:AddComponent("volcanowave")
        local map_x_size, map_y_size = map:GetSize()
        world.WaveComponent:SetWaveParams(13.5, 2.5, -1)
        world.WaveComponent:Init(map_x_size, map_y_size)
        world.WaveComponent:SetWaveSize(80, 3.5)
        world.WaveComponent:SetWaveMotion(3, 0.5, 0.25)
    end

    if world.ismastersim then
        if not Lava_Centre_X or not Lava_Centre_Y or not Lava_Centre_Radius then
            Lava_Centre_X, Lava_Centre_Y, Lava_Centre_Radius = GetLavaCentre()
        end

        if not c_findnext("volcanolavafx") then
            for angle = 0, 2 * math.pi, 2 * math.pi / 6 do
                local r = Lava_Centre_Radius / 2
                local x = Lava_Centre_X + math.cos(angle) * r
                local y = Lava_Centre_Y + math.sin(angle) * r
                local lava = SpawnPrefab("volcanolavafx")
                local lava_edge_light_range = 5
                lava:SetRadius(Lava_Centre_Radius/2  + lava_edge_light_range)
                lava.Transform:SetPosition(x, 0, y)
            end
        end

        world.net:AddComponent("volcanoambience")
    end

    --VolcanoLava(world)
    --world:ListenForEvent("ms_playerjoined", VolcanoLavaHandle)
end)